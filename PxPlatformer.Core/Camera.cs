﻿using System.Drawing;

namespace PxPlatformer.Core
{
    internal class Camera
    {
        private readonly Size screenSize;
        private readonly Point cameraCenter = new Point(0, 0);

        internal Size Offset => new Size(
            screenSize.Width / 2 - cameraCenter.X,
            screenSize.Height / 2 - cameraCenter.Y);

        internal Camera(Size screenSize)
        {
            this.screenSize = screenSize;
        }
    }
}
