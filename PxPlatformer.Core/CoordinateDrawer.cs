﻿using System.Drawing;
using System.Drawing.Text;

namespace PxPlatformer.Core
{
    internal class CoordinateDrawer
    {
        internal DrawableObject DrawCoordinates(Point objectPosition)
        {
            Image image = new Bitmap(100, 25);
            using (Graphics g = Graphics.FromImage(image))
            using (Font font = new Font("Arial", 12))
            {
                g.TextRenderingHint = TextRenderingHint.AntiAlias;
                g.DrawString(objectPosition.ToString(), font, Brushes.Black, Point.Empty);
            }

            return new DrawableObject(image, new Point(objectPosition.X, objectPosition.Y - image.Height));
        }
    }
}
