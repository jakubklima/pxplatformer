﻿using System.Drawing;

namespace PxPlatformer.Core
{
    public static class GameProvider
    {
        public static Game GetGame(Size screenSize)
        {
            var camera = new Camera(screenSize);
            var coordinateDrawer = new CoordinateDrawer();
            return new Game(camera, coordinateDrawer);
        }
    }
}
