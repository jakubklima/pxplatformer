﻿using System.Drawing;

namespace PxPlatformer.Core
{
    public class DrawableObject
    {
        public Image Image { get; }
        public Rectangle Bounds { get; }

        internal DrawableObject(Image image, Point position)
        {
            Image = image;
            Bounds = new Rectangle(position, image.Size);
        }

        internal DrawableObject Offset(Size offset)
        {
            return new DrawableObject(Image, Point.Add(Bounds.Location, offset));
        }
    }
}
