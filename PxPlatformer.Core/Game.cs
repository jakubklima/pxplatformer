﻿using PxPlatformer.Core.Properties;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace PxPlatformer.Core
{
    public class Game
    {
        private readonly Camera camera;
        private readonly CoordinateDrawer coordinateDrawer;
        private readonly List<DrawableObject> drawableObjects = new List<DrawableObject>();

        internal Game(Camera camera, CoordinateDrawer coordinateDrawer)
        {
            this.camera = camera;
            this.coordinateDrawer = coordinateDrawer;

            drawableObjects.Add(CreateTestBlock());
        }

        private DrawableObject CreateTestBlock()
        {
            using (var stream = new MemoryStream(Resources.Block))
            {
                Image image = Image.FromStream(stream);
                return new DrawableObject(image, new Point(100, 0));
            }
        }

        public IEnumerable<DrawableObject> GetDrawableObjects()
        {
            return drawableObjects
                .Concat(drawableObjects.Select(d => coordinateDrawer.DrawCoordinates(d.Bounds.Location)))
                .Select(d => d.Offset(camera.Offset));
        }

        public void Update()
        {
        }
    }
}
