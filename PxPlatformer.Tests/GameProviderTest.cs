﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PxPlatformer.Core;
using System.Drawing;

namespace PxPlatformer.Tests
{
    [TestClass]
    public class GameProviderTest
    {
        [TestMethod]
        public void GetGame_ReturnsNotNull()
        {
            Game game = GameProvider.GetGame(new Size(800, 600));
            Assert.IsNotNull(game);
        }
    }
}
