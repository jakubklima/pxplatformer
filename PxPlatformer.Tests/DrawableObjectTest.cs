﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PxPlatformer.Core;
using System.Drawing;

namespace PxPlatformer.Tests
{
    [TestClass]
    public class DrawableObjectTest
    {
        private static readonly Image image = new Bitmap(100, 200);
        private static readonly Point position = new Point(300, 400);
        private static readonly DrawableObject drawableObject = new DrawableObject(image, position);

        [TestMethod]
        public void Constructor_SetsBounds()
        {
            Assert.AreEqual(image.Size, drawableObject.Bounds.Size);
            Assert.AreEqual(position, drawableObject.Bounds.Location);
        }

        [TestMethod]
        public void Offset_Works()
        {
            Size offset = new Size(200, -200);

            DrawableObject offsetDrawableObject = drawableObject.Offset(offset);

            Assert.AreEqual(drawableObject.Image, offsetDrawableObject.Image);
            Assert.AreEqual(drawableObject.Bounds.Size, offsetDrawableObject.Bounds.Size);
            Assert.AreEqual(new Point(500, 200), offsetDrawableObject.Bounds.Location);
        }
    }
}
