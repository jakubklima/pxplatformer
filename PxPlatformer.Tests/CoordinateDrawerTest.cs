﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PxPlatformer.Core;
using System.Drawing;

namespace PxPlatformer.Tests
{
    [TestClass]
    public class CoordinateDrawerTest
    {
        [TestMethod]
        public void DrawCoordinates_ReturnsInitializedDrawableObject()
        {
            CoordinateDrawer coordinateDrawer = new CoordinateDrawer();
            Point objectPosition = new Point(100, 100);

            DrawableObject coordinates = coordinateDrawer.DrawCoordinates(objectPosition);

            Assert.AreNotEqual(Rectangle.Empty, coordinates.Bounds);
            Assert.IsNotNull(coordinates.Image);
        }
    }
}
