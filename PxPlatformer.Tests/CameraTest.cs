﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using PxPlatformer.Core;
using System.Drawing;

namespace PxPlatformer.Tests
{
    [TestClass]
    public class CameraTest
    {
        [TestMethod]
        public void Offset_Works()
        {
            var camera = new Camera(screenSize: new Size(800, 600));
            Point originalPosition = new Point(100, 200);

            Assert.AreEqual(new Size(400, 300), camera.Offset);
        }
    }
}
