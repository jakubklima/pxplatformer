﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PxPlatformer.Core;
using System.Drawing;
using System.Linq;

namespace PxPlatformer.Tests
{
    [TestClass]
    public class GameTest
    {
        private static Game game = new Game(new Camera(new Size(800, 600)), new CoordinateDrawer());

        [TestMethod]
        public void Update_DoesNotThrow()
        {
            game.Update();
        }

        [TestMethod]
        public void GetDrawableObjects_ReturnsNotNullObjects()
        {
            DrawableObject[] drawableObjects = game.GetDrawableObjects().ToArray();
            Assert.IsTrue(drawableObjects.Any());
            CollectionAssert.AllItemsAreNotNull(drawableObjects);
        }
    }
}
