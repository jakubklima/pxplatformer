﻿using PxPlatformer.Core;
using System;
using System.Reflection;
using System.Windows.Forms;

namespace PxPlatformer.WinForms
{
    public partial class GameForm : Form
    {
        private readonly Game game;

        public GameForm()
        {
            InitializeComponent();
            FixPanelFlickering();
            game = GameProvider.GetGame(panel.Size);
        }

        private void FixPanelFlickering()
        {
            // Based on https://stackoverflow.com/a/15815254
            typeof(Panel).InvokeMember("DoubleBuffered",
                BindingFlags.SetProperty | BindingFlags.Instance | BindingFlags.NonPublic,
                null, panel, new object[] { true });
        }

        private void TimerTick(object sender, EventArgs e)
        {
            game.Update();
            panel.Refresh();
        }

        private void PanelPaint(object sender, PaintEventArgs e)
        {
            foreach (DrawableObject drawableObject in game.GetDrawableObjects())
            {
                e.Graphics.DrawImage(drawableObject.Image, drawableObject.Bounds);
            }
        }
    }
}
